<?php

namespace Drupal\phpmail_alter\Service;

use Drupal\Core\Mail\MailFormatHelper;
use Drupal\Core\Site\Settings;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Transliteration\PhpTransliteration;
use Drupal\Core\File\MimeType\ExtensionMimeTypeGuesser;
use Drupal\Core\File\Exception\FileNotExistsException;
use Symfony\Component\Mime\Header\UnstructuredHeader;

/**
 * Mail backend, using PHP's native mail() function.
 */
class PhpMail implements PhpMailInterface {

  /**
   * The DebugService.
   *
   * @var \Drupal\phpmail_alter\Service\
   */
  // phpcs:disable
  protected DebugServiceInterface $debugService;
  protected ConfigFactoryInterface $configFactory;
  protected ModuleHandlerInterface $moduleHandler;
  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public function __construct(
    DebugServiceInterface $debug,
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler
    ) {
    $this->debugService = $debug;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Concatenates and wraps the email body for plain-text mails.
   */
  public function format(array $message) : string {
    $body = $message['body'];

    // Join the body array into one string.
    $body = implode("\n\n", $body);
    $body = str_replace('<br>', "<br>\r\n", $body);
    // делим длинную строку и вставляем перенос
    $arr = explode("\r\n", $body);
    foreach ($arr as &$value) {
      if (strlen($value) > 400) {
        $pos = stripos($value, ' ', 400);
        $pos = $pos === FALSE ? 400 : $pos;
        $value = substr_replace($value, "\r\n", $pos, 0);
      }
    }
    $body = implode("\r\n", $arr);
    //
    if (substr($message['headers']['Content-Type'], 0, 9) != "text/html") {
      // Convert any HTML to plain-text.
      $body = MailFormatHelper::htmlToText($body);
      // Wrap the mail body for sending.
      $body = MailFormatHelper::wrapMail($body);
    }
    else {
      $body = "<html>\n$body</html>";
    }
    // Note: email uses CRLF for line-endings. PHP's API requires LF on Unix.
    $line_endings = Settings::get('mail_line_endings', PHP_EOL);
    $body = preg_replace('@\r?\n@', $line_endings, $body);

    return $body;
  }

  /**
   * Encode name part of from value.
   *
   * @param string $from_value
   *   From value.
   */
  private function mimeEncodeFromValue(string &$from_value) {
    $from_parts = explode(' <', $from_value);
    if (count($from_parts) == 2) {
      $from_parts[0] = (new UnstructuredHeader('subject', $from_parts[0]))->getBodyAsString();
      $from_value = implode(' <', $from_parts);
    }
  }

  /**
   * Sends an email message.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return bool
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   *
   * @see http://php.net/manual/function.mail.php
   * @see \Drupal\Core\Mail\MailManagerInterface::mail()
   */
  public function mail(array $message) {
    // If 'Return-Path' isn't already set in php.ini, we pass it separately
    // as an additional parameter instead of in the header.
    if (isset($message['headers']['Return-Path'])) {
      $return_path_set = strpos(ini_get('sendmail_path'), ' -f');
      if (!$return_path_set) {
        $message['Return-Path'] = $message['headers']['Return-Path'];
        unset($message['headers']['Return-Path']);
      }
    }
    $mimeheaders = [];
    if (!empty($message['headers']['Reply-to'])) {
      $message['headers']['List-Unsubscribe'] = "<mailto:{$message['headers']['Reply-to']}>";
    }
    foreach ($message['headers'] as $name => $value) {
      if ($name == 'Content-Type' && $value == 'text/html') {
        $value = 'text/html; charset=utf-8';
      }
      if ($name == "From") {
        $this->mimeEncodeFromValue($value);
        $this->moduleHandler->alter('phpmail_alter_from', $value);
        $mimeheaders[] = "$name: $value";
      }
      else {
        $hdr = (new UnstructuredHeader('subject', $value))->getBodyAsString();
        $mimeheaders[] = "$name: $hdr";
      }
    }
    // Prepare mail commands.
    $subject = $message['subject'];
    if (is_object($subject)) {
      $subject = $message['subject']->__toString();
    }
    $mail_subject = (new UnstructuredHeader('subject', $subject))->getBodyAsString();
    $mail_body = $this->format($message);

    // For headers, PHP's API suggests that we use CRLF normally,
    // but some MTAs incorrectly replace LF with CRLF. See #234403.
    $mail_headers = implode("\n", $mimeheaders);
    $additional_headers = '';
    if (isset($message['Return-Path']) && $this->isShellSafe($message['Return-Path'])) {
      $additional_headers = "-f {$message['Return-Path']}";
    }

    $this->appendAttachments($message, $mail_body, $mail_headers);

    $mail_result = @mail(
      $message['to'],
      $mail_subject,
      $mail_body,
      $mail_headers,
      $additional_headers
    );
    if (!$mail_result) {
      $error = t('Unable to send email. Contact the site administrator if the problem persists.');
      \Drupal::messenger()->addError($error);
      $this->debugService->log($message, $mail_headers);
    }
    $this->debugService->debug($message, $mail_headers, $mail_subject, $mail_body, $additional_headers);
    return $mail_result;
  }

  /**
   * Append attachments.
   */
  private function getFilesToAttach(array $message) : array {
    if (empty($message['params']['contact_message'])) {
      return [];
    }
    $contact_message = $message['params']['contact_message'];
    $field_definitions = $contact_message->getFieldDefinitions();
    $files = [];
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($field_definition->getType() != 'file') {
        continue;
      }
      elseif ($contact_message->$field_name->isEmpty()) {
        continue;
      }
      foreach ($contact_message->$field_name as $field_value) {
        $files[] = $field_value->entity;
      }
    }
    return $files;
  }

  /**
   * Append attachments.
   */
  private function appendAttachments(array $message, string &$mail_body, string &$mail_headers) {
    $files = $this->getFilesToAttach($message);
    if (empty($files)) {
      return;
    }

    $boundary = "qd-" . md5("Drupal" . uniqid(time(), TRUE));

    $mimeheaders = [];
    $mimeheaders[] = "Content-Type: multipart/mixed; boundary=\"$boundary\"";
    $headers = $message['headers'];
    if (!empty($headers['Reply-to'])) {
      $headers['List-Unsubscribe'] = "<mailto:{$headers['Reply-to']}>";
    }
    foreach ($headers as $name => $value) {
      if ($name == 'Content-Type') {
        $value = 'text/html; charset=utf-8';
        continue;
      }
      if ($name == "From") {
        $this->moduleHandler->alter('phpmail_alter_from', $value);
        $mimeheaders[] = "$name: $value";
      }
      else {
        $val = (new UnstructuredHeader('subject', $value))->getBodyAsString();
        $mimeheaders[] = "$name: $val";
      }
    }

    $mail_headers = implode("\n", $mimeheaders);

    $lines = "This is a multi-part message in MIME format.\r\n";
    $lines .= "--$boundary\r\n";
    $lines .= "Content-Type: text/html; charset=utf-8 \r\n\r\n";
    $lines .= wordwrap($mail_body) . "\r\n\r\n";

    foreach ($files as $file) {
      $lines .= $this->addAttachment($boundary, [
        'filename' => $file->getFileUri(),
      ]);
    }

    $lines .= "--$boundary--";

    $mail_body = $lines;
  }

  /**
   * Add Attachment.
   */
  private function addAttachment(string $uid, array $aAttachment): string {
    if (empty($aAttachment['filename'])) {
      throw new \InvalidArgumentException("Attachment doesn't have the right structure.");
    }

    if (empty($aAttachment['filecontent'])) {
      if (!is_file($aAttachment['filename'])) {
        throw new FileNotExistsException("Attachment file doesn't exists.");
      }
      $aAttachment['filecontent'] = file_get_contents($aAttachment['filename']);
    }

    $sFileContent = $aAttachment['filecontent'];
    $sChunkedAttachment = chunk_split(base64_encode($sFileContent));

    $transliteration = new PhpTransliteration();
    $filename = $transliteration->transliterate($aAttachment['filename']);
    $sMailAttachmentFriendlyName = basename($filename);

    if (empty($aAttachment['filemime'])) {
      $guesser = new ExtensionMimeTypeGuesser(\Drupal::moduleHandler());
      $aAttachment['filemime'] = $guesser->guess($filename);
    }

    $lines = "--$uid\r\n";
    $lines .= "Content-Type: $aAttachment[filemime];\r\n" .
    $this->encodeHeaderRfc2184('name', $sMailAttachmentFriendlyName);
    $lines .= "Content-Transfer-Encoding: base64\r\n";
    $lines .= "Content-Disposition: attachment;\r\n" .
      $this->encodeHeaderRfc2184('filename', $sMailAttachmentFriendlyName);
    $lines .= "\r\n" . $sChunkedAttachment . "\r\n\r\n";

    return $lines;
  }

  /**
   * Encode Header RFC 2184.
   */
  private function encodeHeaderRfc2184($name, $value) {
    $lines = str_split($value, 64 - strlen($name));
    foreach ($lines as $i => $line) {
      $lines[$i] = "  $name*$i=\"" . $line . "\"";
    }
    return implode(";\r\n", $lines) . "\r\n";
  }

  /**
   * Disallows potentially unsafe shell characters.
   *
   * Functionally similar to PHPMailer::isShellSafe() which resulted from
   * CVE-2016-10045. Note that escapeshellarg and escapeshellcmd are inadequate
   * for this purpose.
   *
   * @param string $string
   *   The string to be validated.
   *
   * @return bool
   *   True if the string is shell-safe.
   *
   * @see https://github.com/PHPMailer/PHPMailer/issues/924
   * @see https://github.com/PHPMailer/PHPMailer/blob/v5.2.21/class.phpmailer.php#L1430
   *
   * @todo Rename to ::isShellSafe() and/or discuss whether this is the correct
   *   location for this helper.
   */
  protected function isShellSafe(string $string) : bool {
    $string_array = ["'$string'", "\"$string\""];
    if (escapeshellcmd($string) !== $string || !in_array(escapeshellarg($string), $string_array)) {
      return FALSE;
    }
    if (preg_match('/[^a-zA-Z0-9@_\-.]/', $string) !== 0) {
      return FALSE;
    }
    return TRUE;
  }

}
