<?php

namespace Drupal\phpmail_alter\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides an interface for PhpMailService.
 */
interface PhpMailInterface {

  /**
   * Constructs a new DebugService object.
   *
   * @param \Drupal\phpmail_alter\Service\DebugServiceInterface $debug
   *   The debug.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    DebugServiceInterface $debug,
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler
  );

}
